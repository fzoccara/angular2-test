import { Injectable }     from '@angular/core';
import { Observable }     from 'rxjs/Observable';
import { Request } from "@angular/http";

@Injectable()
export class FacebookService {

  constructor() {
    if (!window.fbAsyncInit) {
      window.fbAsyncInit = function() {
        FB.init({
          appId: "1697520953801360",
          xfbml: true,
          version: 'v2.5'
        });
      };
    }
  }

  loadAndInitFBSDK():Observable<Request>{
    var id = 'facebook-jssdk',
      ref = document.getElementsByTagName('script')[0];

    if (document.getElementById(id)) {
      return;
    }

    var js = document.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/sdk.js";

    ref.parentNode.insertBefore(js, ref);
  }

}
