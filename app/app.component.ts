import { Component, OnInit, NgZone }  from '@angular/core';
import { FacebookService } from './facebook.service';

import './rxjs-extensions';

@Component({
  selector: 'my-app',
  templateUrl: 'app/app.component.html',
  styleUrls: ['app/app.component.css'],
  providers: [ FacebookService ]
})
export class AppComponent implements OnInit {
  title = 'Tour of Heroes';

  name="";
  isUser = false;
  constructor(
    private _ngZone: NgZone,
    private _facebookService: FacebookService
  ){}

  ngOnInit(){
    this._facebookService.loadAndInitFBSDK();
  }

  login(){
    var self = this;
    FB.login(function(response) {
      if (response.authResponse) {
        FB.api('/me', function(response) {
          self._ngZone.run(() => {
            console.log(response);
            self.name = response.name;
            self.isUser = true
          });
        });
      } else {
        console.log('User cancelled login or did not fully authorize.');
      }
    });
  }
}
